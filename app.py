import json

from flask import Flask, jsonify, make_response, request
from flask_cors import cross_origin

from utils import handle_multipart_request, handle_request

app = Flask(__name__)


@app.route('/', methods=['POST'])
@cross_origin()
def index():
    try:
        files = request.files
        if files:
            form_data = True
            data = request.form
        else:
            form_data = False
            data = json.loads(request.data)

        if form_data:
            result = handle_multipart_request(data, files.getlist('media'))
        else:
            result = handle_request(data)
        return make_response(jsonify(result['data']), result['status'])
    except Exception as e:
        print(e)
        return make_response(jsonify({'message': 'Internal server error'}), 500)
