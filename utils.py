from os import environ
from urllib.parse import urlencode

from requests import get, post

ACCESS_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzbHVnIjoibWxrbWFobXVkIiwicGVyc29uYWxJbmZvIjp7ImVtYWlsIjoibWxrbWFobXVkQHByb3Rvbm1haWwuY29tIiwiZmlyc3ROYW1lIjoiSW5vc3VrZSIsImxhc3ROYW1lIjoiSGFzaGliaXJhIn0sImlhdCI6MTYzNzgyODE0NiwiZXhwIjoxNjM3OTE0NTQ2fQ.zQNJLHrSFJeVmHvZ0L2R2sILvcdJZfFj83KYA6Y1LSg"
BASE_URL = environ.get('PROXY', 'http://localhost:8080')

headers = { 'Authorization': f'Bearer {ACCESS_TOKEN}' }

def parse_response(response):
    if 'application/json' in response.headers['content-type']:
        return {'status': response.status_code, 'data': response.json()}
    else:
        return {'status': response.status_code, 'data': response.text}


def handle_request(payload):
    data = payload.get('data', {})
    method = payload.get('method')
    path = payload.get('path')
    result = {}

    if method.upper() == 'GET':
        query = urlencode(data)
        response = get(f'{BASE_URL}{path}?{query}', headers=headers)
        result = parse_response(response)

    else:
        response = post(f'{BASE_URL}{path}', json=data, headers=headers)
        result = parse_response(response)

    return result

def handle_multipart_request(data, files):
    files_list = [('media', (file)) for file in files]
    response = post(f'{BASE_URL}/api/tutors/upload-media', data=data, files=files_list, headers=headers)
    result = parse_response(response)
    return result
